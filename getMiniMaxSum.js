const getMiniMaxSum = (array) => {
  const oddNumber = [];
  const evenNumber = [];
  const errorResult = "Invalid array";

  let maxNumber = array[0];
  let minNumber = array[0];
  let arraySum = 0;
  let result;

  const isEvenNumber = (value) => value % 2 === 0;

  const isValueInvalid = (value) =>
    typeof value !== "number" || value < 0 || value == Infinity;

  for (let i = 0; i < array.length; i++) {
    if (isValueInvalid(array[i])) {
      result = errorResult;
      break;
    }
    arraySum += array[i];

    if (maxNumber < array[i]) maxNumber = array[i];
    if (minNumber > array[i]) minNumber = array[i];

    isEvenNumber(array[i])
      ? evenNumber.push(array[i])
      : oddNumber.push(array[i]);
  }

  if (result === errorResult) return { result };

  result = `Our minimum sum is ${arraySum - maxNumber} and our maximum sum is ${
    arraySum - minNumber
  }`;

  return {
    result,
    maxNumber,
    minNumber,
    arraySum,
    oddNumber,
    evenNumber,
  };
};
module.exports = getMiniMaxSum;
