const getMiniMaxSum = require("./getMiniMaxSum");

const genResultObject = (
  minimumSum,
  maximumSum,
  maxNumber,
  minNumber,
  arraySum,
  oddNumber,
  evenNumber
) => {
  return {
    result: `Our minimum sum is ${minimumSum} and our maximum sum is ${maximumSum}`,
    maxNumber,
    minNumber,
    arraySum,
    oddNumber,
    evenNumber,
  };
};
describe("Valid input test cases", () => {
  test("Standard case", () => {
    const input = [1, 2, 3, 4, 5];
    const expectedOutput = genResultObject(10, 14, 5, 1, 15, [1, 3, 5], [2, 4]);

    expect(getMiniMaxSum(input)).toStrictEqual(expectedOutput);
  });

  test("Descending input array case", () => {
    const input = [5, 4, 3, 2, 1];
    const expectedOutput = genResultObject(10, 14, 5, 1, 15, [5, 3, 1], [4, 2]);

    expect(getMiniMaxSum(input)).toStrictEqual(expectedOutput);
  });

  test("Unsorted input array case", () => {
    const input = [3, 2, 4, 1, 5];
    const expectedOutput = genResultObject(10, 14, 5, 1, 15, [3, 1, 5], [2, 4]);

    expect(getMiniMaxSum(input)).toStrictEqual(expectedOutput);
  });

  test("input array with duplicated element case", () => {
    const input = [2, 2, 2, 2, 2];
    const expectedOutput = genResultObject(8, 8, 2, 2, 10, [], input);

    expect(getMiniMaxSum(input)).toStrictEqual(expectedOutput);
  });

  test("input array with some element = 0 case", () => {
    const input = [0, 2, 0, 1, 0];
    const expectedOutput = genResultObject(1, 3, 2, 0, 3, [1], [0, 2, 0, 0]);

    expect(getMiniMaxSum(input)).toStrictEqual(expectedOutput);
  });

  test("input array with all element = 0 case", () => {
    const input = [0, 0, 0, 0, 0];
    const expectedOutput = genResultObject(0, 0, 0, 0, 0, [], input);

    expect(getMiniMaxSum(input)).toStrictEqual(expectedOutput);
  });
});

describe("Invalid input test cases", () => {
  const expectedOutput = { result: "Invalid array" };

  test("input array with a element is string case", () => {
    const input = [1, 2, "a", 4, 5];

    expect(getMiniMaxSum(input)).toStrictEqual(expectedOutput);
  });

  test("input array with some element is string case", () => {
    const input = ["b", 2, "a", 4, 5];

    expect(getMiniMaxSum(input)).toStrictEqual(expectedOutput);
  });

  test("input array with a element is undefine case", () => {
    const input = [1, 2, undefined, 4, 5];

    expect(getMiniMaxSum(input)).toStrictEqual(expectedOutput);
  });

  test("input array with a element is null case", () => {
    const input = [1, 2, null, 4, 5];

    expect(getMiniMaxSum(input)).toStrictEqual(expectedOutput);
  });

  test("input array with a element is object case", () => {
    const input = [1, 2, {}, 4, 5];

    expect(getMiniMaxSum(input)).toStrictEqual(expectedOutput);
  });

  test("input array with a element is array case", () => {
    const input = [1, 2, [], 4, 5];

    expect(getMiniMaxSum(input)).toStrictEqual(expectedOutput);
  });

  test("input array with a element is boolean value case", () => {
    const input = [1, 2, true, false, 5];

    expect(getMiniMaxSum(input)).toStrictEqual(expectedOutput);
  });

  test("input array with a element is Infinity case", () => {
    const input = [1, 2, Infinity, 4, 5];

    expect(getMiniMaxSum(input)).toStrictEqual(expectedOutput);
  });

  test("input array with a element is negative value case", () => {
    const input = [1, 2, -1, 4, 5];

    expect(getMiniMaxSum(input)).toStrictEqual(expectedOutput);
  });
});
